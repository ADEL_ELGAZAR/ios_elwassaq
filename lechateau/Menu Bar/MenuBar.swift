//
//  MenuBar.swift
//  lechateau
//
//  Created by mac on 10/03/2024.
//

import Foundation
import UIKit

class MenuBar: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.blue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
