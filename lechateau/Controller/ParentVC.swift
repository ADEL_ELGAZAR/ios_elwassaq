//
//  ParentVC.swift
//  lechateau
//
//  Created by ADEL ELGAZAR on 12/04/2024.
//

import UIKit

class ParentVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.setImage(UIImage.init(named: "ic_back_\(K_Defaults.string(forKey: Saved.language) ?? "en")"), for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton){
        self.dismiss(animated: true)
    }

}
