//
//  WebVC.swift
//  lechateau
//
//  Created by Ahmed Samir on 10/26/21.
//

import UIKit
import WebKit
import MobileRTC
import Toaster

class WebVC: UIViewController, WKUIDelegate {
    
    @IBOutlet var btnBackStep: UIButton! {
        didSet {
            btnBackStep.setImage(UIImage.init(named: "ic_backstep_\(K_Defaults.string(forKey: Saved.language) ?? "en")"), for: .normal)
        }
    }
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet weak var webView: WKWebView!
    var webViewObserver: NSKeyValueObservation?
    
    var site_event_link = ""
    var myURL: URL?
    var meeting: Meeting?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openAppNormally()
    }
    
    func openAppNormally() {
        if !site_event_link.isEmpty {
            myURL = URL(string: site_event_link)
            getUrls(link: site_event_link)
        } else if let url = K_Defaults.string(forKey: "myURL") {
            myURL = URL(string: "https://me-events.me/\(url)")
            getUrls(link: "https://me-events.me/\(url)")
        } else {
            myURL = URL(string: "https://me-events.me/ar/intro")
            getUrls(link: "https://me-events.me/ar/intro")
        }
        K_Defaults.removeObject(forKey: "myURL")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.addObserver(self, forKeyPath:
                                #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
//        webView.configuration.defaultWebpagePreferences.allowsContentJavaScript = false
//        webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = false

        
        // The Zoom SDK requires a UINavigationController to update the UI for us. Here we supplied the SDK with the ViewControllers navigationController.
        MobileRTC.shared().setMobileRTCRootController(self.navigationController)

        /// Notification that is used to start a meeting upon log in success.
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn), name: NSNotification.Name(rawValue: "userLoggedIn"), object: nil)
        
        webViewObserver = webView?.observe(\.url, options: .new, changeHandler: {
                (currentWebView, _) in
                //      Here you go the new path
            if let url = currentWebView.url {
                self.getUrls(link: "\(url)")
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        webViewObserver?.invalidate()
    }
    
    
    // MARK: - Zoom SDK Examples
    /// Puts user into ongoing Zoom meeting using a known meeting number and meeting password.
    ///
    /// Assign a MobileRTCMeetingServiceDelegate to listen to meeting events and join meeting status.
    ///
    /// - Parameters:
    ///   - meetingNumber: The meeting number of the desired meeting.
    ///   - meetingPassword: The meeting password of the desired meeting.
    /// - Precondition:
    ///   - Zoom SDK must be initialized and authorized.
    ///   - MobileRTC.shared().setMobileRTCRootController() has been called.
    func joinMeeting(meetingNumber: String, meetingPassword: String) {
        // Obtain the MobileRTCMeetingService from the Zoom SDK, this service can start meetings, join meetings, leave meetings, etc.
        if let meetingService = MobileRTC.shared().getMeetingService() {

            // Set the ViewContoller to be the MobileRTCMeetingServiceDelegate
            meetingService.delegate = self

            // Create a MobileRTCMeetingJoinParam to provide the MobileRTCMeetingService with the necessary info to join a meeting.
            // In this case, we will only need to provide a meeting number and password.
            let joinMeetingParameters = MobileRTCMeetingJoinParam()
//            joinMeetingParameters.userName = "adelelgazar93+8@gmail.com"
            joinMeetingParameters.meetingNumber = meetingNumber
            joinMeetingParameters.password = meetingPassword

            // Call the joinMeeting function in MobileRTCMeetingService. The Zoom SDK will handle the UI for you, unless told otherwise.
            // If the meeting number and meeting password are valid, the user will be put into the meeting. A waiting room UI will be presented or the meeting UI will be presented.
            meetingService.joinMeeting(with: joinMeetingParameters)
        }
    }

    /// Logs user into their Zoom account using the user's Zoom email and password.
    ///
    /// Assign a MobileRTCAuthDelegate to listen to authorization events including onMobileRTCLoginReturn(_ returnValue: Int).
    ///
    /// - Parameters:
    ///   - email: The user's email address attached to their Zoom account.
    ///   - password: The user's password attached to their Zoom account.
    /// - Precondition:
    ///   - Zoom SDK must be initialized and authorized.
    func logIn(email: String, password: String) {
        // Obtain the MobileRTCAuthService from the Zoom SDK, this service can log in a Zoom user, log out a Zoom user, authorize the Zoom SDK etc.
        if let authorizationService = MobileRTC.shared().getAuthService() {
            // Call the login function in MobileRTCAuthService. This will attempt to log in the user.
//            authorizationService.login(withEmail: email, password: password, rememberMe: false)
        }
    }

    /// Creates and starts a Zoom instant meeting. An instant meeting is an unscheduled meeting that begins instantly.
    ///
    /// Assign a MobileRTCMeetingServiceDelegate to listen to meeting events and start meeting status.
    ///
    /// - Precondition:
    ///   - Zoom SDK must be initialized and authorized.
    ///   - MobileRTC.shared().setMobileRTCRootController() has been called.
    ///   - User has logged into Zoom successfully.
    func startMeeting() {
        // Obtain the MobileRTCMeetingService from the Zoom SDK, this service can start meetings, join meetings, leave meetings, etc.
        if let meetingService = MobileRTC.shared().getMeetingService() {
            // Set the ViewContoller to be the MobileRTCMeetingServiceDelegate
            meetingService.delegate = self

            // Create a MobileRTCMeetingStartParam to provide the MobileRTCMeetingService with the necessary info to start an instant meeting.
            // In this case we will use MobileRTCMeetingStartParam4LoginlUser(), since the user has logged into Zoom.
            let startMeetingParameters = MobileRTCMeetingStartParam4LoginlUser()

            // Call the startMeeting function in MobileRTCMeetingService. The Zoom SDK will handle the UI for you, unless told otherwise.
            meetingService.startMeeting(with: startMeetingParameters)
        }
    }

    var alertController = UIAlertController(title: "Verify Registeration", message: "", preferredStyle: .alert)

    var email = ""
    var name = ""

    // MARK: - Convenience Alerts
    /// Creates alert for prompting the user to enter a meeting number and passcode for joining a meeting.
    func presentJoinMeetingAlert(meeting:Meeting) {
        alertController = UIAlertController(title: "Verify Registeration", message: "", preferredStyle: .alert)
        
        if meeting.lecture_on_off == "on" {
            self.joinMeeting(meetingNumber: meeting.meeting_id, meetingPassword: meeting.passcode)
        } else {
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Email *"
                textField.keyboardType = .emailAddress
                textField.tag = 10
                textField.addTarget(self, action: #selector(WebVC.textFieldDidChange(_:)), for: .editingChanged)
            }
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Name *"
                textField.keyboardType = .default
                textField.tag = 20
                textField.addTarget(self, action: #selector(WebVC.textFieldDidChange(_:)), for: .editingChanged)
            }
            
            let joinMeetingAction = UIAlertAction(title: "Join meeting", style: .default, handler: { alert -> Void in
                if meeting.lecture_on_off.elementsEqual("on") || meeting.emails.contains(self.email) {
                    self.joinMeeting(meetingNumber: meeting.meeting_id, meetingPassword: meeting.passcode)
                } else {
                    self.showNotAuthDialog()
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
            
            alertController.addAction(joinMeetingAction)
            alertController.addAction(cancelAction)
            joinMeetingAction.isEnabled = false
            
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func showNotAuthDialog() {

        // create the alert
        let alert = UIAlertController(title: "Unauthorized", message: "Not authorized to join", preferredStyle: UIAlertController.Style.alert)

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 10 {
            email = textField.text ?? ""
        } else if textField.tag == 20 {
            name = textField.text ?? ""
        }

        if !email.isEmpty && !name.isEmpty {
            (alertController.actions[0] as UIAlertAction).isEnabled = true
        } else {
            (alertController.actions[0] as UIAlertAction).isEnabled = false
        }
    }

    /// Creates alert for prompting the user to enter thier Zoom credentials for starting a meeting.
    func presentLogInAlert() {
        let alertController = UIAlertController(title: "Log in", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Email"
            textField.keyboardType = .emailAddress
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Password"
            textField.keyboardType = .asciiCapable
            textField.isSecureTextEntry = true
        }

        let logInAction = UIAlertAction(title: "Log in", style: .default, handler: { alert -> Void in
            let emailTextField = alertController.textFields![0] as UITextField
            let passwordTextField = alertController.textFields![1] as UITextField

            if let email = emailTextField.text, let password = passwordTextField.text {
                self.logIn(email: email, password: password)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })

        alertController.addAction(logInAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }

    // MARK: - Internal
    /// Selector that is used to start a meeting upon log in success.
    @objc func userLoggedIn() {
        startMeeting()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print("\(webView.estimatedProgress)")
            progressView.progress = Float(webView.estimatedProgress)
            if webView.estimatedProgress == 1.0 {
                progressView.isHidden = true
            } else {
                progressView.isHidden = false
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        progressView.isHidden = true
    }
    
    @IBAction func btnRefreshPressed(_ sender: Any) {
        webView.reload()
    }
    
    @IBAction func btnBackStepPressed(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func btnClosePressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let frame = navigationAction.targetFrame,
            frame.isMainFrame {
            return nil
        }
        webView.load(navigationAction.request)
        return nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle .darkContent
    }
    
    /// setupSDK Creates, initializes, and authorizes an instance of the Meeting SDK. Call this before calling any other SDK functions.
    /// - Parameters:
    ///   - jwtToken: A valid generated JWT.
    func setupSDK(jwtToken: String) {
        // Create a MobileRTCSDKInitContext. This class contains attributes for determining how the SDK will be used. You must supply the context with a domain.
        let context = MobileRTCSDKInitContext()
        // The domain we will use is zoom.us
        context.domain = "zoom.us"
        // Turns on SDK logging. This is optional.
        context.enableLog = true
        // Call initialize(_ context: MobileRTCSDKInitContext) to create an instance of the Zoom SDK. Without initialization, the SDK will not be operational. This call will return true if the SDK was initialized successfully.
        let sdkInitializedSuccessfully = MobileRTC.shared().initialize(context)
        // Check if initialization was successful. Obtain a MobileRTCAuthService, this is for supplying credentials to the SDK for authorization.
        if let authorizationService = MobileRTC.shared().getAuthService() {
            // Supply the SDK with a JWT.
            authorizationService.jwtToken = jwtToken
            // Assign AppDelegate to be a MobileRTCAuthDelegate to listen for authorization callbacks.
            authorizationService.delegate = self
            // Call sdkAuth to perform authorization.
            authorizationService.sdkAuth()
        }
    }

}

// 1. Extend the ViewController class to adopt and conform to MobileRTCMeetingServiceDelegate. The delegate methods will listen for updates from the SDK about meeting connections and meeting states.
extension WebVC: MobileRTCMeetingServiceDelegate {

    // Is called upon in-meeting errors, join meeting errors, start meeting errors, meeting connection errors, etc.
    func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        switch error {
        case MobileRTCMeetError.passwordError:
            print("Could not join or start meeting because the meeting password was incorrect.")
        default:
            print("Could not join or start meeting with MobileRTCMeetError: \(error) \(message ?? "")")
        }
    }

    // Is called when the user joins a meeting.
    func onJoinMeetingConfirmed() {
        print("Join meeting confirmed.")
    }

    // Is called upon meeting state changes.
    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        print("Current meeting state: \(state)")
    }
}

// MARK: - MobileRTCAuthDelegate
// Conform AppDelegate to MobileRTCAuthDelegate.
// MobileRTCAuthDelegate listens to authorization events like SDK authorization, user login, etc.
extension WebVC: MobileRTCAuthDelegate {
    // Result of calling sdkAuth(). MobileRTCAuthError_Success represents a successful authorization.
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        switch returnValue {
        case .success:
            print("SDK successfully initialized.")
            guard let meeting = meeting else { return }
            self.presentJoinMeetingAlert(meeting: meeting)
        case .tokenWrong:
            assertionFailure("JWT Token Authentication is invalid.")
        default:
            assertionFailure("SDK Authorization failed with MobileRTCAuthError: \(returnValue).")
        }
    }
}

extension WebVC {
    
    func getUrls(link: String) {
        K_Network.sendRequest(function: apiService.getUrls(link: link), showLoading: false, success: { (code, msg, response)  in
            do {
                self.meeting = try response.map(to: Meeting.self, keyPath: "data")
                self.getSignature(meetingNumber: self.meeting?.meeting_id ?? "")
            } catch {
            }
            
        }) { (code, msg, response, errors) in
        }
    }
    
    func getSignature(meetingNumber: String) {
        K_Network.sendRequest(function: tokenService.getSignature(meetingNumber: meetingNumber), showLoading: false, success: { (code, msg, response)  in
            do {
                let user = try response.map(to: User.self)
                self.setupSDK(jwtToken: user.signature)
            } catch {
            }
            
        }) { (code, msg, response, errors) in
        }
    }

}
