//
//  VerifyRegisterInputParams.swift
//  lechateau
//
//  Created by mac on 02/03/2024.
//

import Foundation

struct VerifyRegisterInputParams: Encodable {
    var email: String
    var verificationCode: String
}

struct SendEmailVerifyCodeInputParams: Encodable {
    var email: String
    var type: String
}
