//
//  EditProfilleInputParams.swift
//  lechateau
//
//  Created by mac on 10/03/2024.
//

import Foundation

struct EditProfilleInputParams: Encodable {
    var title: String
    var email: String
    var name: String
    var phone: String
}
