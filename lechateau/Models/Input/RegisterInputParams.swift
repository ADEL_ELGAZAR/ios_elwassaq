//
//  RegisterInputParams.swift
//  lechateau
//
//  Created by mac on 02/03/2024.
//

import Foundation

struct RegisterInputParams: Encodable {
    var title: String
    var name: String
    var title_ar: String
    var name_ar: String
    var phone: String
    var job_title: String
    var specilty_type: String
    var classification_job_id: String
    var email: String
    var password: String
    var password_confirmation: String
    var locale: String
}
