//
//  RestPasswordInputParams.swift
//  lechateau
//
//  Created by mac on 02/03/2024.
//

import Foundation

struct RestPasswordInputParams: Encodable {
    var email: String
    var password: String
    var password_confirmation: String
    var verificationCode: String
}
