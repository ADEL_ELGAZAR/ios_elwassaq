//
//  LoginParams.swift
//  lechateau
//
//  Created by mac on 02/03/2024.
//

import Foundation

struct LoginInputParams: Encodable {
    var email: String
    var password: String
}
