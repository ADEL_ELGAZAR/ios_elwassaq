//
//  TeamMember.swift
//  Maly7a-Provider
//
//  Created by Sayed Reda on 2/7/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Mapper

class Meeting:Mappable {
//    var lecture_link = ""
    var meeting_id = ""
    var lecture_on_off = ""
    var passcode = ""
    var emails:[String] = []

    required init (map: Mapper) throws {
//        lecture_link = map.optionalFrom("lecture_link") ?? ""
        meeting_id = map.optionalFrom("meeting_id") ?? ""
        lecture_on_off = map.optionalFrom("lecture_on_off") ?? ""
        passcode = map.optionalFrom("passcode") ?? ""
        emails = map.optionalFrom("emails") ?? []
    }
}
