//
//  TeamMember.swift
//  Maly7a-Provider
//
//  Created by Sayed Reda on 2/7/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Mapper

class Contact:Mappable {
    var address = ""
    var phone = ""
    var email = ""

    required init (map: Mapper) throws {
        address = map.optionalFrom("address") ?? ""
        phone = map.optionalFrom("phone") ?? ""
        email = map.optionalFrom("email") ?? ""
    }
}
