//
//  TeamMember.swift
//  Maly7a-Provider
//
//  Created by Sayed Reda on 2/7/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Mapper

class About:Mappable {
    var ourstory = ""
    var ourvision = ""
    var ourmsg = ""
    var ourmission = ""
    var ourvalues = ""

    required init (map: Mapper) throws {
        ourstory = map.optionalFrom("ourstory") ?? ""
        ourvision = map.optionalFrom("ourvision") ?? ""
        ourmsg = map.optionalFrom("ourmsg") ?? ""
        ourmission = map.optionalFrom("ourmission") ?? ""
        ourvalues = map.optionalFrom("ourvalues") ?? ""
    }
}
