//
//  Event.swift
//  lechateau
//
//  Created by mac on 11/03/2024.
//

import Foundation
import Mapper

class Event: Mappable {
    var id = 0
    var logo = ""
    var main_image = ""
    var url_image = ""
    var external_url = ""
    var site_event_link = ""
    var slideshow = ""
    var is_slideshow = 0
    var type_id = 0
    var section_id = 0
    var country_id = 0
    var country = ""
    var city_id = 0
    var city = ""
    var place_id = 0
    var lectuers_schedule = ""
    var from_date = ""
    var to_date = ""
    var category_ids = ""
    var partners = ""
    var hours = 0
    var youtube = ""
    var amount = 0
    var user_id = ""
    var certificate_logo = ""
    var certificate_text = ""
    var certificate_signature = ""
    var group_type = ""
    var manger_type = ""
    var lecturer_type = ""
    var arbitrators_evaluators_type = ""
    var created_at = ""
    var updated_at = ""
    var break_date = ""
    var subscribe_alert_image1 = ""
    var subscribe_alert_image2 = ""
    var event_image_id = ""
    var event_image_id_pdf = ""
    var name = ""
    var summary = ""
    var admin_word = ""
    var subscription_fees = ""
    var late_string = ""
    var early_string = ""
    var translations: [TranslationsDetalis] = []
    var event: Event?
    var mobile_image = ""

    required init (map: Mapper) throws {
        id = map.optionalFrom("id") ?? 0
        logo = map.optionalFrom("logo") ?? ""
        main_image = map.optionalFrom("main_image") ?? ""
        url_image = map.optionalFrom("url_image") ?? ""
        external_url = map.optionalFrom("external_url") ?? ""
        site_event_link = map.optionalFrom("site_event_link") ?? ""
        slideshow = map.optionalFrom("slideshow") ?? ""
        is_slideshow = map.optionalFrom("is_slideshow") ?? 0
        type_id = map.optionalFrom("type_id") ?? 0
        section_id = map.optionalFrom("section_id") ?? 0
        country_id = map.optionalFrom("country_id") ?? 0
        country = map.optionalFrom("country") ?? ""
        city_id = map.optionalFrom("city_id") ?? 0
        city = map.optionalFrom("city") ?? ""
        place_id = map.optionalFrom("place_id") ?? 0
        lectuers_schedule = map.optionalFrom("lectuers_schedule") ?? ""
        from_date = map.optionalFrom("from_date") ?? ""
        to_date = map.optionalFrom("to_date") ?? ""
        category_ids = map.optionalFrom("category_ids") ?? ""
        partners = map.optionalFrom("partners") ?? ""
        hours = map.optionalFrom("hours") ?? 0
        youtube = map.optionalFrom("youtube") ?? ""
        amount = map.optionalFrom("amount") ?? 0
        user_id = map.optionalFrom("user_id") ?? ""
        certificate_logo = map.optionalFrom("certificate_logo") ?? ""
        certificate_text = map.optionalFrom("certificate_text") ?? ""
        certificate_signature = map.optionalFrom("certificate_signature") ?? ""
        group_type = map.optionalFrom("group_type") ?? ""
        manger_type = map.optionalFrom("manger_type") ?? ""
        lecturer_type = map.optionalFrom("lecturer_type") ?? ""
        arbitrators_evaluators_type = map.optionalFrom("arbitrators_evaluators_type") ?? ""
        created_at = map.optionalFrom("created_at") ?? ""
        updated_at = map.optionalFrom("updated_at") ?? ""
        break_date = map.optionalFrom("break_date") ?? ""
        subscribe_alert_image1 = map.optionalFrom("subscribe_alert_image1") ?? ""
        subscribe_alert_image2 = map.optionalFrom("subscribe_alert_image2") ?? ""
        event_image_id = map.optionalFrom("event_image_id") ?? ""
        event_image_id_pdf = map.optionalFrom("event_image_id_pdf") ?? ""
        name = map.optionalFrom("name") ?? ""
        summary = map.optionalFrom("summary") ?? ""
        admin_word = map.optionalFrom("admin_word") ?? ""
        subscription_fees = map.optionalFrom("subscription_fees") ?? ""
        late_string = map.optionalFrom("late_string") ?? ""
        early_string = map.optionalFrom("early_string") ?? ""
        translations = map.optionalFrom("translations") ?? []
        event = map.optionalFrom("event")
        mobile_image = map.optionalFrom("mobile_image") ?? ""
    }
    
    init(){
        
    }
}

class TranslationsDetalis: Mappable {
    var id = 0
    var event_id = 0
    var locale = ""
    var name = ""
    var summary = ""
    var admin_word = ""
    var created_at = ""
    var updated_at = ""
    var subscription_fees = ""
    var late_string = ""
    var early_string = ""
    required init(map: Mapper) throws {
        id = map.optionalFrom("id") ?? 0
        event_id = map.optionalFrom("event_id") ?? 0
        locale = map.optionalFrom("locale") ?? ""
        name = map.optionalFrom("name") ?? ""
        summary = map.optionalFrom("summary") ?? ""
        created_at = map.optionalFrom("created_at") ?? ""
        updated_at = map.optionalFrom("updated_at") ?? ""
        subscription_fees = map.optionalFrom("subscription_fees") ?? ""
        late_string = map.optionalFrom("late_string") ?? ""
        early_string = map.optionalFrom("early_string") ?? ""
    }
}

class CurrentEvent: Mappable {
    
    var id = 0
    var event_id = 0
    var bag = ""
    var meal = 0
    var user_id = ""
    var qrcode_string = ""
    var event: Event?
    
    required init(map: Mapper) throws {
        id = map.optionalFrom("id") ?? 0
        event_id = map.optionalFrom("event_id") ?? 0
        bag = map.optionalFrom("bag") ?? ""
        meal = map.optionalFrom("meal") ?? 0
        user_id = map.optionalFrom("user_id") ?? ""
        qrcode_string = map.optionalFrom("qrcode_string") ?? ""
        event = map.optionalFrom("event")
    }
}
