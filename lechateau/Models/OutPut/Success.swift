//
//  Success.swift
//  lechateau
//
//  Created by mac on 04/03/2024.
//

import Foundation
import Mapper

class Success: Mappable {
    var message = ""
    var status = false
    required init (map: Mapper) throws {
        message = map.optionalFrom("message") ?? ""
        status = map.optionalFrom("status") ?? false
    }
}



