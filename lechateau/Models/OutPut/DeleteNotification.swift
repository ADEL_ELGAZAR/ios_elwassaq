//
//  DeleteNotification.swift
//  lechateau
//
//  Created by mac on 06/04/2024.
//

import Foundation
import Mapper


class DeleteNotification: Mappable {
    var message = ""
    var status = false
    required init (map: Mapper) throws {
        status = map.optionalFrom("status") ?? false
        message = map.optionalFrom("message") ?? ""
    }
}

class ReadNotification: Mappable {
    var message = ""
    var status = false
    required init (map: Mapper) throws {
        status = map.optionalFrom("status") ?? false
        message = map.optionalFrom("message") ?? ""
    }
}

class ShowNotification: Mappable {
    var id = ""
    var read_at = ""
    var title = ""
    var details = ""
    var type = ""
    var event = ""
    var date = ""
    
    required init(map: Mapper) throws {
        id = map.optionalFrom("id") ?? ""
        read_at = map.optionalFrom("read_at") ?? ""
        title = map.optionalFrom("title") ?? ""
        details = map.optionalFrom("details") ?? ""
        type = map.optionalFrom("type") ?? ""
        event = map.optionalFrom("event") ?? ""
        date = map.optionalFrom("date") ?? ""
    }
}
