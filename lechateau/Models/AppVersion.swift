//
//  TeamMember.swift
//  Maly7a-Provider
//
//  Created by Sayed Reda on 2/7/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Mapper

class AppVersion:Mappable {
    var status = false
    var message = ""
    var data = ""

    required init (map: Mapper) throws {
        status = map.optionalFrom("status") ?? false
        message = map.optionalFrom("message") ?? ""
        data = map.optionalFrom("data") ?? ""
    }
}
