//
//  TeamMember.swift
//  Maly7a-Provider
//
//  Created by Sayed Reda on 2/7/18.
//  Copyright © 2018 hassan. All rights reserved.
//

import Foundation
import Mapper

class User: Mappable {
    var signature = ""
    var id = 0
    var token = ""
    var title = ""
    var name = ""
    var email = ""
    var phone = ""
    var message = ""
    var emailVerified = false
    
    var title_ar = ""
    var name_ar = ""
    var job_title = ""
    var specilty_type = 0
    var classification_job_id = 0
    var user_type = ""
    var created_at = ""
    var updated_at = ""
    var nationality = ""
    var country = ""
    var gender = ""
    var national_id = 0
    var profession_card = ""
    var country_id = 0
    var city_id = 0
    var state = ""
    var postal_code = 0
    var interests = ""
    var cv = ""
    var bio = ""
    var user_org = ""
    var org_number = ""
    var email_verified_at = ""
    var avatar = ""
    var messenger_color = ""
    var dark_mode = ""
    var exhibition_booth_id = ""
    var association_number = ""
    var work_location = ""
    var active_status = 0
    var sorting = ""
    var certification_status = ""
    var identification_card = ""
    
    required init (map: Mapper) throws {
        signature = map.optionalFrom("signature") ?? ""
        id = map.optionalFrom("id") ?? 0
        token = map.optionalFrom("token") ?? ""
        title = map.optionalFrom("title") ?? ""
        name = map.optionalFrom("name") ?? ""
        email = map.optionalFrom("email") ?? ""
        phone = map.optionalFrom("phone") ?? ""
        message = map.optionalFrom("message") ?? ""
        emailVerified = map.optionalFrom("emailVerified") ?? false
        
        title_ar = map.optionalFrom("title_ar") ?? ""
        name_ar = map.optionalFrom("name_ar") ?? ""
        job_title = map.optionalFrom("job_title") ?? ""
        specilty_type = map.optionalFrom("specilty_type") ?? 0
        classification_job_id = map.optionalFrom("classification_job_id") ?? 0
        user_type = map.optionalFrom("user_type") ?? ""
        created_at = map.optionalFrom("created_at") ?? ""
        updated_at = map.optionalFrom("updated_at") ?? ""
        nationality = map.optionalFrom("nationality") ?? ""
        country = map.optionalFrom("country") ?? ""
        gender = map.optionalFrom("gender") ?? ""
        national_id = map.optionalFrom("national_id") ?? 0
        profession_card = map.optionalFrom("profession_card") ?? ""
        country_id = map.optionalFrom("country_id") ?? 0
        city_id = map.optionalFrom("city_id") ?? 0
        state = map.optionalFrom("state") ?? ""
        postal_code = map.optionalFrom("postal_code") ?? 0
        interests = map.optionalFrom("interests") ?? ""
        cv = map.optionalFrom("cv") ?? ""
        bio = map.optionalFrom("bio") ?? ""
        user_org = map.optionalFrom("user_org") ?? ""
        org_number = map.optionalFrom("org_number") ?? ""
        email_verified_at = map.optionalFrom("email_verified_at") ?? ""
        avatar = map.optionalFrom("avatar") ?? ""
        messenger_color = map.optionalFrom("messenger_color") ?? ""
        dark_mode = map.optionalFrom("dark_mode") ?? ""
        exhibition_booth_id = map.optionalFrom("exhibition_booth_id") ?? ""
        association_number = map.optionalFrom("association_number") ?? ""
        work_location = map.optionalFrom("work_location") ?? ""
        active_status = map.optionalFrom("active_status") ?? 0
        sorting = map.optionalFrom("sorting") ?? ""
        certification_status = map.optionalFrom("certification_status") ?? ""
        identification_card = map.optionalFrom("identification_card") ?? ""
        
    }
}

      
