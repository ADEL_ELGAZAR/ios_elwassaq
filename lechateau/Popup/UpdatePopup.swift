//
//  DriverSplash.swift
//  Deliy
//
//  Created by Youm7 on 10/9/17.
//  Copyright © 2017 binshakerr. All rights reserved.
//

import UIKit
import Moya

class UpdatePopup: UIView {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var btnUpdate: UIView!
    @IBOutlet weak var lblUpdateMessage: UILabel!
    
    class func instanceFromNib() -> UpdatePopup {
        return UINib(nibName: "UpdatePopup", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UpdatePopup
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.container.endEditing(true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        container.layer.cornerRadius = 5
        btnUpdate.layer.cornerRadius = 19
        btnUpdate.layer.masksToBounds = false
        btnUpdate.layer.shadowOpacity = 0.5
        btnUpdate.layer.shadowRadius = 5
        
    }
    
    @IBAction func btnUpdatePressed(_ sender: Any) {
        if let url = URL(string: "https://itunes.apple.com/in/app/middle-east-events-app/id1595620122") {
           if #available(iOS 10.0, *) {
               UIApplication.shared.open(url, options: [:], completionHandler: nil)
           } else {
               // Earlier versions
               if UIApplication.shared.canOpenURL(url as URL) {
                  UIApplication.shared.openURL(url as URL)
               }
           }
        }
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
            exit(EXIT_SUCCESS);
    }
    
    func DismissView(){
        UIView.animate(withDuration: 0.15, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.container.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) {_ in
            self.removeFromSuperview()
        }
    }
    
}
