//
//  AppDelegate.swift
//  lechateau
//
//  Created by Ahmed Samir on 10/26/21.
//

import UIKit
import MobileRTC
import IQKeyboardManagerSwift
import Firebase
import MOLH
import FirebaseCore
import FirebaseAnalytics
import FirebaseMessaging
import FirebaseDynamicLinks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    //1. Add UIWindow
    var window: UIWindow?
    let navigator = Navigator()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        MOLHLanguage.setDefaultLanguage("en")
        MOLH.shared.activate(true)
        
        if #available(iOS 13.0, *) { window!.overrideUserInterfaceStyle = .light }
        setupKeyboardManager()
        FirebaseApp.configure()
        enableNotification(application: application)
        reset()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    func subscribeToTopic(topicName: String) {
        Messaging.messaging().subscribe(toTopic: topicName) { error in
            if let error = error {
                print("Error subscribing to topic: \(error.localizedDescription)")
            } else {
                print("Subscribed to topic: \(topicName)")
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Obtain the MobileRTCAuthService from the Zoom SDK, this service can log in a Zoom user, log out a Zoom user, authorize the Zoom SDK etc.
        if let authorizationService = MobileRTC.shared().getAuthService() {
            
            // Call logoutRTC() to log the user out.
            authorizationService.logoutRTC()
        }
    }
    
    private func setupKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        guard let url = userActivity.webpageURL else { return false }
        if "\(url)".contains("https://me-events.me/") {
            K_Defaults.set("\(url)".replacingOccurrences(of: "https://me-events.me/", with: ""), forKey: "myURL")
        }
        let handled = DynamicLinks.dynamicLinks()
            .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
                
            }
        
        guard let viewController = navigator.getDestination(for: url) else {
            application.open(url)
            return false
        }
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
        return handled
    }
    
    var rootViewController: UIViewController? {
        return window?.rootViewController
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        
        K_Defaults.set("\(url)".components(separatedBy: "://").last, forKey: "myURL")
        //        restartApplication()
        return true
    }
}

extension AppDelegate: MOLHResetable {
    func reset() {
        var vc: UIViewController?
//        if (K_Defaults.string(forKey: Saved.language) ?? "").isEmpty {
            vc = PromoVC()
//        } else if !K_Defaults.bool(forKey: Saved.infoSkipped) {
//            vc = AppInfoVC()
//        } else if (K_Defaults.string(forKey: Saved.api_token) ?? "").isEmpty {
//            vc = LoginVC()
//        } else {
//            vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabController")
//        }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
}

// MARK: - Notifications
extension AppDelegate: UNUserNotificationCenterDelegate {
    func enableNotification(application: UIApplication) {

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]

            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })

        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        // For iOS 10 data message (sent via FCM)
        Messaging.messaging().delegate = self

        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let token = token {
                K_Defaults.set(token, forKey: Saved.fcm_token)
            }
        }
        connectToFcm()

        NotificationCenter.default.addObserver(self, selector:#selector(tokenRefreshNotification(notification:)), name: NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)

    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        subscribeToTopic(topicName: "me_event")
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint(error)
    }

    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        if Auth.auth().canHandleNotification(notification) {
//            completionHandler(UIBackgroundFetchResult.noData)
//            return
//        }

        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        print("Push notification received:\n\(notification.jsonString ?? "{}")")
        Messaging.messaging().appDidReceiveMessage(notification)
        // This notification is not auth related, developer should handle it.
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {

    }

    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received:\n\(data.jsonString ?? "{}")")

    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .sound])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        guard let userInfo = response.notification.request.content.userInfo as? [String: Any] else {return}
        print("Push notification received:\n\(userInfo.jsonString ?? "{}")")

//        if let type = userInfo["type"] as? String {
//            Constants.uDefaults.set(type, forKey: Constants.notificationType)
//            switch type {
//            case "ad":
//                // open ViewVehicleVC
//                let adId = userInfo["ad_id"] as! String
//                let adTitle = userInfo["title"] as! String
//                Constants.uDefaults.set(Int(adId), forKey: Constants.notificationAdId)
//                Constants.uDefaults.set(adTitle, forKey: Constants.notificationAdTitle)
//                Constants.uDefaults.set(true, forKey: Constants.isFromNotifications)
//                let storyboard = UIStoryboard(name: StoryBoardName.vehicle.rawValue, bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardVCIds.viewVehicle.rawValue) as! ViewVehicleVC
//                vc.vehicleId = Int(adId)!
//                vc.vehicleTitle = adTitle
//                vc.backToHome = true
//                Constants.uWindow?.rootViewController = vc
//                Constants.uWindow?.makeKeyAndVisible()
//                AppCoordinator.shared.clearNukeCache()
//            default:
//                break
//            }
//        }
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        // Note: This callback is fired at each app startup and whenever a new token is generated.

        let token = Messaging.messaging().fcmToken
        print("Firebase registration token FCM:\n\(token ?? "nil")")

        if let token = token {
            K_Defaults.set(token, forKey: Saved.fcm_token)
        }
    }

    @objc func tokenRefreshNotification(notification: NSNotification) {
        // NOTE: It can be nil here
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let token = token {
                K_Defaults.set(token, forKey: Saved.fcm_token)
            }
        }
        connectToFcm()
    }

    func connectToFcm() {
        Messaging.messaging().isAutoInitEnabled = true
    }
}

extension Dictionary {
    /// Utility method for printing Dictionaries as pretty-printed JSON.
    var jsonString: String? {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted]),
            let jsonString = String(data: jsonData, encoding: .utf8) {
            return jsonString
        }
        return nil
    }
}
