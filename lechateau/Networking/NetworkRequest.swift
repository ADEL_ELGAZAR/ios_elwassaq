//
//  NetworkRequest.swift
//  ProductiveFamilies
//
//  Created by ADEL ELGAZAR on 10/3/20.
//  Copyright © 2020 ADEL ELGAZAR. All rights reserved.
//

import Foundation
import Moya
import Mapper
import Moya_ModelMapper
import ARSLineProgress

class NetworkRequest {
    
    /// responsible for sending requests to server in all application and handling the result in case of success or fail
    /// plus it containing loading popup and control showing and hiding it
    /// - Parameters:
    ///   - function: which request you need to call
    ///   - showLoading: show loading popup or not
    ///   - success: callback occur when response is success
    ///   - failure: callback occur when response is fail
    /// - Returns: nil
    func sendRequest(function:apiService, showLoading:Bool = true, success:@escaping(_ code:String, _ msg:String, _ response :Response)->(), failure:@escaping (_ code:String, _ msg:String, _ response :Response, _ errors:NetworkValidationError?)->()) {
        
        if showLoading {
            ARSLineProgress.show()
        }
        let provider = MoyaProvider<apiService>()
        provider.request(function) { (result) in
            if showLoading {
                ARSLineProgress.hide()
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    do {
                        let Dic = try response.mapJSON() as! [String: Any]
                        switch response.statusCode {
                        case 0:
                            success("", "", response)
                        case 100:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 101:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 200:
                            success("\(response.statusCode)", "\(Dic["message"] ?? "")", response)
                        case 201:
                            success("\(response.statusCode)", "\(Dic["message"] ?? "")", response)
                        case 204:
                            success("\(response.statusCode)", "\(Dic["message"] ?? "")", response)
                        case 400:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 401:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 403:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 404:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 422:
                            let errors = try response.map(to: NetworkValidationError.self, keyPath: "errors")
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, errors)
                        case 500:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 600:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        default:
                            failure("\(response.statusCode)", "", response, nil)
                        }
                    } catch {
                        failure("", "", response, nil)
                    }
                case .failure( _):
                    return
                }
            }
        }
    }
    
    /// responsible for sending requests to server in all application and handling the result in case of success or fail
    /// plus it containing loading popup and control showing and hiding it
    /// - Parameters:
    ///   - function: which request you need to call
    ///   - showLoading: show loading popup or not
    ///   - success: callback occur when response is success
    ///   - failure: callback occur when response is fail
    /// - Returns: nil
    func sendRequest(function:tokenService, showLoading:Bool = true, success:@escaping(_ code:String, _ msg:String, _ response :Response)->(), failure:@escaping (_ code:String, _ msg:String, _ response :Response, _ errors:NetworkValidationError?)->()) {
        
        if showLoading {
            ARSLineProgress.show()
        }
        let provider = MoyaProvider<tokenService>()
        provider.request(function) { (result) in
            if showLoading {
                ARSLineProgress.hide()
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    do {
                        let Dic = try response.mapJSON() as! [String: Any]
                        switch response.statusCode {
                        case 0:
                            success("", "", response)
                        case 100:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 101:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 200:
                            success("\(response.statusCode)", "\(Dic["message"] ?? "")", response)
                        case 400:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 401:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 403:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 404:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 422:
                            let errors = try response.map(to: NetworkValidationError.self, keyPath: "errors")
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, errors)
                        case 500:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        case 600:
                            failure("\(response.statusCode)", "\(Dic["message"] ?? "")", response, nil)
                        default:
                            failure("\(response.statusCode)", "", response, nil)
                        }
                    } catch {
                    }
                case .failure( _):
                    return
                }
            }
        }
    }
}


/// this model is made to handle validation messages from the server
public class NetworkValidationError:Mappable {
    var title: [String] = []
    var name: [String] = []
    var phone: [String] = []
    var job_title: [String] = []
    var specilty_type: [String] = []
    var classification_job_id: [String] = []
    var email: [String] = []
    var password: [String] = []
    var password_confirmation: [String] = []
    var verificationCode: [String] = []
    var locale: [String] = []
    
    required public init (map: Mapper) throws {
        title = map.optionalFrom("title") ?? []
        name = map.optionalFrom("name") ?? []
        phone = map.optionalFrom("phone") ?? []
        job_title = map.optionalFrom("job_title") ?? []
        specilty_type = map.optionalFrom("specilty_type") ?? []
        classification_job_id = map.optionalFrom("classification_job_id") ?? []
        email = map.optionalFrom("email") ?? []
        password = map.optionalFrom("password") ?? []
        password_confirmation = map.optionalFrom("password_confirmation") ?? []
        verificationCode = map.optionalFrom("verificationCode") ?? []
        locale = map.optionalFrom("locale") ?? []
    }
}
