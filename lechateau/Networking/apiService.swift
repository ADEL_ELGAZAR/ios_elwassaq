//
//  apiService.swift
//  TaqeemTeacher
//
//  Created by ADEL ELGAZAR on 4/14/20.
//  Copyright © 2020 ADEL ELGAZAR. All rights reserved.
//

import Moya

enum apiService {
    case getContact
    case getAbout
    case getUrls(link: String)
    case CheckVersion(version: String)
    case logout
    case login(params: LoginInputParams)
    case register(params: RegisterInputParams)
    case verifyRegister(params: VerifyRegisterInputParams)
    case sendEmailverifyCode(params: SendEmailVerifyCodeInputParams)
    case resetPassword(params: RestPasswordInputParams)
    case getProfile
    case editProfile(params: EditProfilleInputParams)
    case getAllEvents
    case getMyEvents
    case comingEvents
    case currentEvents
    case showNotifications
    case readAllNotifications
    case deleteAllNotifications
    case deleteSingleNotification(id: String)
    case myCurrnetEvent
    case acceptBag(id: Int)
    case getOldEvents
}

// MARK: - TargetType Protocol Implementation
extension apiService: TargetType {
    var baseURL: URL {
        switch self {
        case .getContact, .getAbout, .getUrls, .CheckVersion:
            return URL(string: K_API_URL)!
        default:
            let language = K_Defaults.string(forKey: Saved.language) ?? "en"
            return URL(string: language.elementsEqual("en") ? K_EN_URL : K_AR_URL)!
        }
    }
    
    var path: String {
        switch self {
        case .getContact:
            return "/contact"
        case .getAbout:
            return "/about"
        case .logout:
            return "/auth/logout"
        case .getUrls:
            return "/zoomInfo"
        case .CheckVersion:
            return "/CheckVersion"
        case .login:
            return "/auth/login"
        case .register:
            return "/auth/register"
        case .verifyRegister:
            return "/auth/register/verify"
        case .sendEmailverifyCode:
            return "/auth/verify/send"
        case .resetPassword:
            return "/auth/password/reset"
        case .getProfile:
            return "/profile"
        case .editProfile:
            return "/profile"
        case .getAllEvents:
            return "/events"
        case .getMyEvents:
            return "/events/me"
        case .showNotifications:
            return "/notifications/index"
        case .readAllNotifications:
            return "/notifications/read-notifications"
        case .deleteAllNotifications:
            return "/notifications/delete-user-notifications"
        case .deleteSingleNotification(let id):
            return "/notifications/delete-single-notifications/\(id)"
        case .comingEvents:
            return "/coming-events"
        case .currentEvents:
            return "/current-events"
        case .myCurrnetEvent:
            return "/events/me/current"
        case .acceptBag(let id):
            return "/events/\(id)/bag"
        case .getOldEvents:
            return "/old-events"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getContact, .getAbout, .getUrls, .CheckVersion , .getProfile , .getAllEvents , .getMyEvents ,.showNotifications , .readAllNotifications , .comingEvents , .currentEvents , .myCurrnetEvent, .acceptBag , .getOldEvents:
            return .get
        case .editProfile:
            return .patch
        case .deleteAllNotifications , .deleteSingleNotification:
            return .delete
        default:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        var parameters = [String: Any]()
        switch self {
        case .getUrls(let link):
            parameters["link"] = link
            return parameters
        case .CheckVersion(let version):
            parameters["version"] = version
            return parameters
        case .login(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .register(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .verifyRegister(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .sendEmailverifyCode(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .resetPassword(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .editProfile(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        case .deleteSingleNotification(let params):
            parameters = params.toDictionary ?? [:]
            return parameters
        default:
            return [String: Any]()
        }
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
    
    var task: Task {
        switch self {
        case .getContact, .getAbout, .getUrls, .CheckVersion , .getProfile, .myCurrnetEvent, .getAllEvents, .getMyEvents , .deleteSingleNotification, .comingEvents , .currentEvents , .showNotifications , .readAllNotifications, .acceptBag , .getOldEvents:
            return .requestParameters(parameters: parameters!, encoding: URLEncoding.queryString)
            
        default:
            return .requestParameters(parameters: parameters!, encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        var parameters = [String: String]()
        let token = K_Defaults.string(forKey: Saved.api_token) ?? ""
        parameters["Authorization"] = "Bearer \(token)"
        if !token.isEmpty {
            parameters["fcm-token"] = "\(K_Defaults.string(forKey: Saved.fcm_token) ?? "")"
        }
        parameters["Accept"] = "application/json"
        return parameters
    }
    
    var sampleData: Data {
        return Data()
    }
    
}
