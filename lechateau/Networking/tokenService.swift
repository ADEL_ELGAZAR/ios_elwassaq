//
//  apiService.swift
//  TaqeemTeacher
//
//  Created by ADEL ELGAZAR on 4/14/20.
//  Copyright © 2020 ADEL ELGAZAR. All rights reserved.
//

import Moya

enum tokenService {
    case getSignature(meetingNumber: String)
    case login(params: LoginInputParams)
}

// MARK: - TargetType Protocol Implementation
extension tokenService: TargetType {
    var baseURL: URL { return URL(string: K_SIGN_URL)! }
    
    var path: String {
        switch self {
        case .getSignature:
            return "/getSignature"
        case .login:
            return "/auth/login"
        }
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getSignature(let meetingNumber):
            var parameters = [String: Any]()
            parameters["meetingNumber"] = meetingNumber
            parameters["role"] = 0
            return parameters
        default:
            return [String: Any]()
        }
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters!, encoding: JSONEncoding.default)
    }
    
    var headers: [String: String]? {
        var parameters = [String: String]()
//        parameters["Authorization"] = "Bearer \(K_Defaults.string(forKey: Saved.api_token) ?? "")"
//        parameters["Accept-Language"] = "ar"
        return parameters
    }
    
    var sampleData: Data {
        return Data()
    }
    
}
