//
//  DeeplinkHandlerProtocol.swift
//  lechateau
//
//  Created by mac on 20/12/2022.
//

import Foundation

protocol DeeplinkHandlerProtocol {
    func canOpenURL(_ url: URL) -> Bool
    func openURL(_ url: URL)
}
