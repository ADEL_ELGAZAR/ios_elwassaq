//
//  Extensions.swift
//  Deliy
//
//  Created by Sayed Reda on 11/13/17.
//  Copyright © 2017 binshakerr. All rights reserved.
//
import UIKit

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension Date {
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
    func dateString(_ format: String = "MMM-dd-YYYY, hh:mm a") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    func dateByAddingYears(_ dYears: Int) -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.year = dYears
        
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
}

extension UIImage {
    func resizeImage() -> UIImage {
        let image = self
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 500.0
        let maxWidth: Float = 500.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}

extension UIImage {
    
    func fixedOrientation() -> UIImage? {
           
        guard imageOrientation != UIImage.Orientation.up else {
               //This is default orientation, don't need to do anything
               return self.copy() as? UIImage
           }
           
           guard let cgImage = self.cgImage else {
               //CGImage is not available
               return nil
           }

           guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
               return nil //Not able to create CGContext
           }
           
           var transform: CGAffineTransform = CGAffineTransform.identity
           
           switch imageOrientation {
           case .down, .downMirrored:
               transform = transform.translatedBy(x: size.width, y: size.height)
               transform = transform.rotated(by: CGFloat.pi)
               break
           case .left, .leftMirrored:
               transform = transform.translatedBy(x: size.width, y: 0)
               transform = transform.rotated(by: CGFloat.pi / 2.0)
               break
           case .right, .rightMirrored:
               transform = transform.translatedBy(x: 0, y: size.height)
               transform = transform.rotated(by: CGFloat.pi / -2.0)
               break
           case .up, .upMirrored:
               break
           }
           
           //Flip image one more time if needed to, this is to prevent flipped image
           switch imageOrientation {
           case .upMirrored, .downMirrored:
               transform.translatedBy(x: size.width, y: 0)
               transform.scaledBy(x: -1, y: 1)
               break
           case .leftMirrored, .rightMirrored:
               transform.translatedBy(x: size.height, y: 0)
               transform.scaledBy(x: -1, y: 1)
           case .up, .down, .left, .right:
               break
           }
           
           ctx.concatenate(transform)
           
           switch imageOrientation {
           case .left, .leftMirrored, .right, .rightMirrored:
               ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
           default:
               ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
               break
           }
           
           guard let newCGImage = ctx.makeImage() else { return nil }
           return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
       }
}

extension String {
    func toAttributed(alignment: NSTextAlignment) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return toAttributed(attributes: [.paragraphStyle: paragraphStyle])
    }

    func toAttributed(attributes: [NSAttributedString.Key : Any]? = nil) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    func toDate() -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = formatter.date(from: self) {
            return date
        }
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        if let date = formatter.date(from: self) {
            return date
        }
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        if let date = formatter.date(from: self) {
            return date
        }
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: self)
    }
    
    private static let formatter = NumberFormatter()

        func clippingCharacters(in characterSet: CharacterSet) -> String {
            components(separatedBy: characterSet).joined()
        }

        func convertedDigitsToLocale(_ locale: Locale = .current) -> String {
            let digits = Set(clippingCharacters(in: CharacterSet.decimalDigits.inverted))
            guard !digits.isEmpty else { return self }

            Self.formatter.locale = locale
            let maps: [(original: String, converted: String)] = digits.map {
                let original = String($0)
                guard let digit = Self.formatter.number(from: String($0)) else {
                    assertionFailure("Can not convert to number from: \(original)")
                    return (original, original)
                }
                guard let localized = Self.formatter.string(from: digit) else {
                    assertionFailure("Can not convert to string from: \(digit)")
                    return (original, original)
                }
                return (original, localized)
            }

            var converted = self
            for map in maps { converted = converted.replacingOccurrences(of: map.original, with: map.converted) }
            return converted
        }
    
    func toLatin() -> String {
        return self.convertedDigitsToLocale(Locale(identifier: "EN"))
    }
}

extension Date {
    func toString(format:String = "yyyy-MM-dd HH:mm") -> String {
        // Create Date Formatter
        let dateFormatter = DateFormatter()

        // Set Date Format
        dateFormatter.dateFormat = format

        // Convert Date to String
        return dateFormatter.string(from: self)
    }
    
    func toUTC() -> String {
        return ISO8601DateFormatter().string(from: self)
    }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}
