//
//  Constants.swift
//  Deliy
//
//  Created by Youm7 on 10/2/17.
//  Copyright © 2017 binshakerr. All rights reserved.
//

import UIKit

let K_Main_URL = "https://me-events.me"
let K_API_URL = K_Main_URL + "/api"
let K_EN_URL = K_Main_URL + "/api/en/v1"
let K_AR_URL = K_Main_URL + "/api/ar/v1"
let K_SIGN_URL = "https://us-central1-events-app-e80f6.cloudfunctions.net/"
let K_Device_ID = UIDevice.current.identifierForVendor!.uuidString
let K_Defaults = UserDefaults.standard
let K_Notifications = NotificationCenter.default
let K_AppDelegate = UIApplication.shared.delegate as! AppDelegate
let K_Parse_Error = "Something is not correct, please try again later"
let K_Server_Error = "Something happened. Please try again later"
let K_Invalid_Access = "Please, Complete Data First."
var timer:Timer?
var minutes = Array(1...59).map { "\($0)" }
var K_Network = NetworkRequest()
var K_CollectionCellWidth = (UIScreen.main.bounds.width-33)/2
var K_CollectionCellHeight = (K_CollectionCellWidth*4)/3

enum Saved { //keys of userDefaults
    static let api_token = "api_token"
    static let fcm_token = "fcm_token"
    static let name = "name"
    static let phone = "phone"
    static let email = "email"
    static let image = "image"
    static let confirm = "confirm"
    static let id = "id"
    static let language = "language"
    static let infoSkipped = "infoSkipped"
    static let notifications = "notifications"
}

enum ProjectStatus {
    static let inActive = 0
    static let active = 1
}

enum DisplayType {
    case None
    case View
    case Add
    case Edit
    case Fast
}

enum StoryBoards {
    case Main

    var storyboard: UIStoryboard {
        switch self {
        case .Main:
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
    func viewController(identifier: String) -> UIViewController {
        return self.storyboard.instantiateViewController(withIdentifier: identifier)
    }
}
