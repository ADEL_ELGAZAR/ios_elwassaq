//
//  Navigator.swift
//  lechateau
//
//  Created by mac on 22/12/2022.
//

import Foundation
import UIKit

struct Navigator {
    func getDestination(for url: URL) -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let tabBarController = storyboard.instantiateInitialViewController() as? UITabBarController
        tabBarController?.selectedIndex = 0
        return tabBarController
    }
}
