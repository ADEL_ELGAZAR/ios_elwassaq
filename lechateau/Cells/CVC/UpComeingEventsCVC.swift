//
//  UpComeingEventsCVC.swift
//  lechateau
//
//  Created by mac on 07/04/2024.
//

import UIKit

class UpComeingEventsCVC: UICollectionViewCell {
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventLocation: UILabel!
    
    func configre(event: Event){
        loadImage(stringUrl: event.mobile_image, placeHolder: nil, imgView: imgEvent)
        lblEventName.text = event.name
        lblEventLocation.text = "\(event.city), \(event.country)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = extractDateComponents(from: event.from_date)
        lblDay.text = "\(date?.day ?? 0)"
        lblMonth.text = "\(date?.month ?? "")"
    }
    
    func extractDateComponents(from dateString: String) -> (day: Int, month: String, year: Int)? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // Adjust the format according to your input string

        if let date = dateFormatter.date(from: dateString) {
            let calendar = Calendar.current
            let components = calendar.dateComponents([.day, .month, .year], from: date)
            
            if let day = components.day, let _ = components.month, let year = components.year {
                // Format month name
                let monthFormatter = DateFormatter()
                monthFormatter.dateFormat = "MMM"
                let monthName = monthFormatter.string(from: date)
                
                return (day, monthName, year)
            }
        }
        
        return nil
    }
}
