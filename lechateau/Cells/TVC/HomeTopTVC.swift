//
//  HomeTopTVC.swift
//  lechateau
//
//  Created by mac on 07/04/2024.
//

import UIKit
import SwiftGifOrigin

class HomeTopTVC: UITableViewCell {
    
    @IBOutlet weak var viewRegisterAttendance: UIView!
    @IBOutlet weak var viewGoToLecture: UIView!
    @IBOutlet weak var viewEventsActionParent: UIView!
    @IBOutlet weak var registerAttendanceGif: UIImageView!
    
    var event: CurrentEvent?
    var context: UIViewController?
    
    func ConfigreTopView() {
        registerAttendanceGif.image = UIImage.gif(name: "qr-code 1")
        viewRegisterAttendance.onTap {
            let vc = RegisterAttendanceVC()
            vc.modalPresentationStyle = .fullScreen
            vc.event = self.event
            self.context?.present(vc, animated: true)
        }
        
        viewGoToLecture.onTap {
            let vc = WebVC()
            vc.site_event_link = self.event?.event?.site_event_link ?? ""
            vc.modalPresentationStyle = .fullScreen
            self.context?.present(vc, animated: true)
        }
    }
}
