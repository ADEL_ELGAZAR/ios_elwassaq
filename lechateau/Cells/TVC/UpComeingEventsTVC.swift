//
//  UpComeingEventsTVC.swift
//  lechateau
//
//  Created by mac on 07/04/2024.
//

import UIKit

class UpComeingEventsTVC: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var events: [Event] = []
    
    func configureEvent(title: String , events: [Event]){
        lblTitle.text = title
        collectionView.register(UINib(nibName: "UpComeingEventsCVC", bundle: nil), forCellWithReuseIdentifier: "UpComeingEventsCVC")
        collectionView.delegate = self
        collectionView.dataSource = self
        self.events = events
        collectionView.reloadData()
    }
}

extension UpComeingEventsTVC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        let CellWidth = (collectionView.frame.width - layout.minimumInteritemSpacing) / 2
        return CGSize(width: CellWidth, height: CellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpComeingEventsCVC", for: indexPath) as? UpComeingEventsCVC else {return UICollectionViewCell()}
        cell.configre(event: events[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = WebVC()
        vc.site_event_link = events[indexPath.row].site_event_link
        vc.modalPresentationStyle = .fullScreen
        parentContainerViewController()?.present(vc, animated: true)
    }
    
}
