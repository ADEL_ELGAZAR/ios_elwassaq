//
//  EventTVC.swift
//  lechateau
//
//  Created by mac on 11/03/2024.
//

import UIKit

class EventTVC: UITableViewCell {
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDataFrom: UILabel!
    @IBOutlet weak var lblDateTo: UILabel!
    
    func configure(event: Event){
        imgEvent.layer.cornerRadius = 50
        loadImage(stringUrl: event.mobile_image, placeHolder: UIImage(named: "leChateauLogo"), imgView: imgEvent)
        lblName.text = event.name
        lblDataFrom.text = event.from_date
        lblDateTo.text = event.to_date
    }
}
    
   
