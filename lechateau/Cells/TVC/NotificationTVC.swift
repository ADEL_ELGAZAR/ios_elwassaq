//
//  NotificationTVC.swift
//  lechateau
//
//  Created by mac on 18/04/2024.
//

import UIKit

class NotificationTVC: UITableViewCell {

    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblEvent: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewReadNotification: UIView!
    
    
    func configureNotification(notification: ShowNotification){
//        loadImage(stringUrl: notification.logo, placeHolder: UIImage(named: "leChateauLogo"), imgView: imgNotification)
        lblTitle.text = notification.title
        lblDetails.text = notification.details
        lblType.text = notification.type
        lblEvent.text = notification.event
        lblDate.text = notification.date
        viewReadNotification.isHidden = !notification.read_at.isEmpty
    }
}

