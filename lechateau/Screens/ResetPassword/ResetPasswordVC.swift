//
//  ResetPasswordVC.swift
//  lechateau
//
//  Created by mac on 05/03/2024.
//

import UIKit
import Toaster
import OTPFieldView

class ResetPasswordVC: ParentVC, OTPFieldViewDelegate {

    @IBOutlet var otpTextFieldView: OTPFieldView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    var email = ""
    var otpCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
    }

    func setupOtpView(){
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 3
        self.otpTextFieldView.borderColor = UIColor(named: "036C96Opacity")!
        self.otpTextFieldView.filledBorderColor = UIColor(named: "036C96Opacity")!
        self.otpTextFieldView.displayType = .roundedCorner
        self.otpTextFieldView.fieldSize = 50
        self.otpTextFieldView.separatorSpace = 4
        self.otpTextFieldView.shouldAllowIntermediateEditing = true
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
        
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        otpCode = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("Has entered all OTP? \(hasEnteredAll)")
        return true
    }
    
    @IBAction func btnConfirmPressed(_ sender: UIButton){
            resetPassword(params: RestPasswordInputParams(email: email, password: txtPassword.text ?? "", password_confirmation: txtConfirmPassword.text ?? "", verificationCode: otpCode))
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton){
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    @IBAction func btnShowConfirmPassword(_ sender: UIButton){
        txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
    }
    
    @IBAction func btnResendCode(_ sender: UIButton){
        sendVerifyCode(params: SendEmailVerifyCodeInputParams(email: "\(email)", type: "password"))
    }
}


extension ResetPasswordVC {
    
    func resetPassword(params: RestPasswordInputParams) {
        K_Network.sendRequest(function: apiService.resetPassword(params: params), showLoading: true ,success: { (code, msg, response)  in
            do {
                let user = try response.map(to: User.self, keyPath: "data")
                K_Defaults.set(user.name, forKey: Saved.name)
                K_Defaults.set(user.phone, forKey: Saved.phone)
                K_Defaults.set(user.token, forKey: Saved.api_token)
                K_Defaults.set(user.email, forKey: Saved.email)
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"tabController")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            } catch {
                print("Error Here")
            }
            
        }) { (code, msg, response, errors) in
            if errors?.password.first != nil {
                Toast(text: errors?.password[0]).show()
            } 
            else if errors?.verificationCode.first != nil {
                Toast(text: errors?.verificationCode[0]).show()
            }
        }
    }
    
    func sendVerifyCode(params: SendEmailVerifyCodeInputParams) {
        K_Network.sendRequest(function: apiService.sendEmailverifyCode(params: params), showLoading: true ,success: { (code, msg, response)  in
            do {
                let message = try response.map(to: Success.self)
                Toast(text: "Resend Success".localized()).show()
            } catch {
                print("Error Here")
            }
            
        }) { (code, msg, response, errors) in
            if errors?.email.first != nil {
                Toast(text: errors?.email[0]).show()
            }
        }
    }
}
