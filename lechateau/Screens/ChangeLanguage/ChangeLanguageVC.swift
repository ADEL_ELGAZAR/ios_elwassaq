//
//  ChangeLanguageVC.swift
//  lechateau
//
//  Created by mac on 06/04/2024.
//

import UIKit
import MOLH

class ChangeLanguageVC: ParentVC {
    
    @IBOutlet weak var viewLanguage: UIView!
    @IBOutlet weak var lblLanguage: UILabel!
    
    var languages: [String] = ["English - اللغة الإنجليزية", "Arabic - اللغة العربية"]
    var backHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.isHidden = backHidden
        
        viewLanguage.onTap(action: {
            DropDown(vc: self, array: self.languages.map({ $0 })).show(selecteditem: [self.lblLanguage.text ?? ""], showSearchBar: false, didSelect: { (text, selected, selectedItems)  in
                self.lblLanguage.text = text
                self.lblLanguage.tag = self.languages.map({ $0 }).firstIndex(of: text ?? "") ?? 0
            })
        })
    }
    
    @IBAction func btnConfirmPressed(_ sender: UIButton){
        K_Defaults.set(self.lblLanguage.tag == 1 ? "ar" : "en", forKey: Saved.language)
        MOLH.setLanguageTo(self.lblLanguage.tag == 1 ? "ar" : "en")
        MOLH.reset()
    }
}
