//
//  HomeVC.swift
//  lechateau
//
//  Created by mac on 07/04/2024.
//

import UIKit
import SYBadgeButton
import Toaster

class HomeVC: ParentVC {
    
    @IBOutlet weak var tabelView: UITableView!
    @IBOutlet weak var btnNotification: SYBadgeButton!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblGreating: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserAvater: UIImageView!
    
    @IBOutlet weak var viewEventDate: UIView!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var lblEventDescription: UILabel!
    
    var userName = ""
    var userImage = ""
    var currentEvents: [Event] = []
    var commingEvents: [Event] = []
    var oldEvents: [Event] = []
    var event: CurrentEvent?
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh".localized())
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tabelView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getProfile()
        getMyCurrentEvent()
        getCommingEvents()
        getCurrentEvents()
        getOldEvents()
        showNotifications()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewDidAppear(false)
        refreshControl.endRefreshing()
    }
    
    func setupUI(){
        if let notifications = K_Defaults.string(forKey: Saved.notifications), !notifications.isEmpty {
            btnNotification.badgeValue = notifications
        }
        btnNotification.setImage(UIImage(named: "notification 1"), for: .normal)
        tabelView.register(UINib(nibName: "HomeTopTVC", bundle: nil), forCellReuseIdentifier: "HomeTopTVC")
        tabelView.register(UINib(nibName: "UpComeingEventsTVC", bundle: nil), forCellReuseIdentifier: "UpComeingEventsTVC")
        
        // Create a date formatter to extract the hour component
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        
        // Convert the hour string to an integer
        if let hour = Int(dateFormatter.string(from: Date())) {
            if hour < 12 {
                lblGreating.text = "good_morning".localized()
            } else {
                lblGreating.text = "good_evening".localized()
            }
        } else {
            print("Error: Unable to determine current time")
        }
    }
    
    @IBAction func btnNotificationsPressed(_ : UIButton) {
        let vc = NotificationsVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func extractDateComponents(from dateString: String) -> (day: Int, month: String, year: Int)? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // Adjust the format according to your input string
        
        if let date = dateFormatter.date(from: dateString) {
            let calendar = Calendar.current
            let components = calendar.dateComponents([.day, .month, .year], from: date)
            
            if let day = components.day, let _ = components.month, let year = components.year {
                // Format month name
                let monthFormatter = DateFormatter()
                monthFormatter.dateFormat = "MMM"
                let monthName = monthFormatter.string(from: date)
                
                return (day, monthName, year)
            }
        }
        
        return nil
    }
    
    func refreshEvent() {
        if let event = self.event {
            self.viewEventDate.isHidden = false
            self.lblEventTitle.text = "You_are_registered".localized()
            self.lblEventDescription.text = event.event?.name ?? ""
            let date = self.extractDateComponents(from: event.event?.from_date ?? "")
            self.lblDay.text = "\(date?.day ?? 0)"
            self.lblDate.text = "\(date?.month ?? "") \(date?.year ?? 0)"
        } else {
            self.viewEventDate.isHidden = true
            self.lblEventTitle.text = "You_are_not_registered".localized()
        }
        self.tabelView.reloadData()
    }
}


extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTopTVC", for: indexPath) as! HomeTopTVC
            if let event = self.event {
                cell.event = event
            }
            cell.ConfigreTopView()
            cell.context = self
            return cell
        case 1 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpComeingEventsTVC", for: indexPath) as! UpComeingEventsTVC
            cell.configureEvent(title: "current_events".localized(), events: self.currentEvents)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpComeingEventsTVC", for: indexPath) as! UpComeingEventsTVC
            cell.configureEvent(title: "upcoming_events".localized(), events: self.commingEvents)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpComeingEventsTVC", for: indexPath) as! UpComeingEventsTVC
            cell.configureEvent(title: "old_events".localized(), events: self.oldEvents)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0 && event == nil) ||
            (indexPath.row == 1 && currentEvents.count == 0) ||
            (indexPath.row == 2 && commingEvents.count == 0) ||
            (indexPath.row == 3 && oldEvents.count == 0) {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension HomeVC {
    func getProfile() {
        K_Network.sendRequest(function: apiService.getProfile, showLoading: false ,success: { (code, msg, response)  in
            do {
                let user = try response.map(to: GetProfile.self, keyPath: "data")
                self.lblUserName.text = user.name
                self.loadImage(stringUrl: user.avatar, placeHolder: UIImage(named: "leChateauLogo"), imgView: self.imgUserAvater)
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func getMyCurrentEvent() {
        K_Network.sendRequest(function: apiService.myCurrnetEvent, showLoading: true ,success: { (code, msg, response)  in
            do {
                self.event = try response.map(to: CurrentEvent.self, keyPath: "data")
                self.refreshEvent()
                
            } catch {
                self.refreshEvent()
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func getCommingEvents() {
        K_Network.sendRequest(function: apiService.comingEvents, showLoading: false ,success: { (code, msg, response)  in
            do {
                self.commingEvents = try response.map(to: [Event].self, keyPath: "data")
                self.tabelView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func getCurrentEvents() {
        K_Network.sendRequest(function: apiService.currentEvents, showLoading: false ,success: { (code, msg, response)  in
            do {
                self.currentEvents = try response.map(to: [Event].self, keyPath: "data")
                self.tabelView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func getOldEvents() {
        K_Network.sendRequest(function: apiService.getOldEvents, showLoading: false ,success: { (code, msg, response)  in
            do {
                self.oldEvents = try response.map(to: [Event].self, keyPath: "data")
                self.tabelView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func showNotifications() {
        K_Network.sendRequest(function: apiService.showNotifications, showLoading: false ,success: { (code, msg, response)  in
            do {
                let notifications = try response.map(to: [ShowNotification].self, keyPath: "notifications")
                let count = notifications.filter({ $0.read_at.isEmpty }).count
                self.lblNotificationCount.text = "\(count)"
                self.lblNotificationCount.superview?.isHidden = count == 0
                UIApplication.shared.applicationIconBadgeNumber = count
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
}

extension UICollectionView {

    public func scrollToLastItem(at scrollPosition: UICollectionView.ScrollPosition, adjustment: CGFloat = 0.0, withAdjustmentDuration duration: TimeInterval = 0.5) {
        let lastSection = self.numberOfSections - 1
        let lastRowInLastSection = self.numberOfItems(inSection: lastSection)
        if lastSection > 0, lastRowInLastSection > 0 {
            let indexPath = IndexPath(row: lastRowInLastSection - 1, section: lastSection)
            let visibleIndexPaths = self.indexPathsForVisibleItems
            if !visibleIndexPaths.contains(indexPath) {
                self.self.scrollToItem(at: indexPath, at: scrollPosition, animated: true)
                UIView.animate(withDuration: duration) {
                    switch scrollPosition {
                    case .top, .bottom, .centeredVertically:
                        self.contentOffset.y += adjustment
                    case .left, .right, .centeredHorizontally:
                        self.contentOffset.x += adjustment
                    default:
                        print("Inavlid scrollPosition: \(scrollPosition)")
                    }
                }
            }
        }
    }
}

extension UICollectionViewFlowLayout {

    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        let isAr = K_Defaults.string(forKey: Saved.language) == "ar"
        if isAr {
            collectionView?.semanticContentAttribute = .forceRightToLeft
            return true
        } else {
            collectionView?.semanticContentAttribute = .forceLeftToRight
            return false
        }
    }

}
