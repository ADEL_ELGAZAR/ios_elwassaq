//
//  EditProfileVC.swift
//  lechateau
//
//  Created by mac on 10/03/2024.
//

import UIKit
import Toaster

class EditProfileVC: ParentVC, UITextFieldDelegate {
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.delegate = self
        getProfile()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            return (textField.text! as NSString).replacingCharacters(in: range, with: string).isValidSaudiPhone()
        }
        return true
    }
    
    @IBAction func btnSavePressed(_ sender: UIButton){
        editProfile(params: EditProfilleInputParams(title: txtTitle.text ?? "", email: txtEmail.text ?? "", name: txtName.text ?? "", phone: txtPhone.text ?? ""))
    }
}

extension EditProfileVC {
    func getProfile() {
        K_Network.sendRequest(function: apiService.getProfile, showLoading: true ,success: { (code, msg, response)  in
            do {
                let user = try response.map(to: User.self, keyPath: "data")
                self.txtTitle.text = user.title
                self.txtName.text = user.name
                self.txtPhone.text = user.phone
                self.txtEmail.text = user.email
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func editProfile(params: EditProfilleInputParams) {
        K_Network.sendRequest(function: apiService.editProfile(params: params), showLoading: true ,success: { (code, msg, response)  in
            
            self.dismiss(animated: true)
            
        }) { (code, msg, response, errors) in
            if errors?.email.first != nil {
                Toast(text: errors?.email[0]).show()
            }
            else if errors?.name.first != nil {
                Toast(text: errors?.name[0]).show()
            }
            else if errors?.phone.first != nil {
                Toast(text: errors?.phone[0]).show()
            }
        }
    }
}
