//
//  ProfileVC.swift
//  lechateau
//
//  Created by mac on 10/03/2024.
//

import UIKit

class ProfileVC: ParentVC {
    
    @IBOutlet weak var personalDataView: UIView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet weak var privacyView: UIView!
    @IBOutlet weak var languageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        personalDataView.onTap(action: personalDataViewPressed )
        aboutUsView.onTap(action: aboutUsViewPressed)
        contactUsView.onTap(action: contactUsViewPressed)
        privacyView.onTap(action: privacyViewPressed)
        languageView.onTap(action: languageViewPressed)
    }
    
    func personalDataViewPressed(){
        let vc = EditProfileVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func aboutUsViewPressed(){
        let vc = AboutUsVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func contactUsViewPressed(){
        let vc = ContactUsVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func privacyViewPressed(){
        let vc = PrivacyVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    func languageViewPressed(){
        let vc = ChangeLanguageVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func btnLogoutPressed(_ : UIButton){
        logout()
    }
    
    @IBAction func btnDeleteAccountPressed(_ : UIButton){
        logout()
    }
    
    func logoutLocally() {
        K_Defaults.removeObject(forKey: Saved.api_token)
        let vc = LoginVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
}

extension ProfileVC {
    func logout() {
        K_Network.sendRequest(function: apiService.logout, showLoading: true ,success: { (code, msg, response)  in
            self.logoutLocally()
        }) { (code, msg, response, errors) in
            self.logoutLocally()
        }
    }
    
    func deleteNotification(id: String) {
        K_Network.sendRequest(function: apiService.deleteSingleNotification(id: id), showLoading: true ,success: { (code, msg, response)  in
            do {
                let response = try response.map(to: DeleteNotification.self , keyPath: nil)
                print("\(response.message)")
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            
        }
    }
    
    func deleteAllNotifications(){
        K_Network.sendRequest(function: apiService.deleteAllNotifications, showLoading: true ,success: { (code, msg, response)  in
            do {
                let response = try response.map(to: DeleteNotification.self , keyPath: nil)
                print("\(response.message)")
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            
        }
    }
}
