//
//  RegisterAttendanceVC.swift
//  lechateau
//
//  Created by mac on 08/04/2024.
//

import UIKit
import SimpleQRCode
import Toaster

class RegisterAttendanceVC: ParentVC {
    
    @IBOutlet weak var codeImage: UIImageView!
    @IBOutlet weak var loaderGif: UIImageView!
    @IBOutlet weak var eventLogo: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var imgUserAvater: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var btnAcceptBag: UIButton!
    
    var event: CurrentEvent?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getProfile()
        loadImage(stringUrl: event?.event?.logo ?? "", placeHolder: nil, imgView: self.eventLogo)
        let qrCodeB = QRCode(string: event?.qrcode_string ?? "")
        codeImage.image =  try? qrCodeB?.image()
        loaderGif.image = UIImage.gif(name: "ezgif_speed")
        
        lblEventName.text = event?.event?.name ?? ""
        lblLocation.text = "\(event?.event?.city ?? ""), \(event?.event?.country ?? "")"
        lblStartDate.text = event?.event?.from_date ?? ""
        lblEndDate.text = event?.event?.to_date ?? ""
        if event?.bag == "1" {
            btnAcceptBag.setTitle("confirm_receipt".localized(), for: .normal)
            btnAcceptBag.isUserInteractionEnabled = true
            btnAcceptBag.alpha = 1.0
        } else if event?.bag == "2" {
            btnAcceptBag.isUserInteractionEnabled = false
            btnAcceptBag.alpha = 1.0
            btnAcceptBag.backgroundColor = UIColor.init(named: "7ABD59")
            btnAcceptBag.setTitle("delivered".localized(), for: .normal)
            btnAcceptBag.setImage(UIImage(named: "12608_active_check_checkmark_correct_done_icon"), for: .normal)
        } else {
            btnAcceptBag.setTitle("confirm_receipt".localized(), for: .normal)
            btnAcceptBag.isUserInteractionEnabled = false
            btnAcceptBag.alpha = 0.5
        }
    }
    
    @IBAction func btnConfirmPressed(_ sender: UIButton) {
        acceptBag(id: event?.id ?? 0)
    }
}

extension RegisterAttendanceVC {
    
    func getProfile() {
        K_Network.sendRequest(function: apiService.getProfile, showLoading: false ,success: { (code, msg, response)  in
            do {
                let user = try response.map(to: GetProfile.self, keyPath: "data")
                self.lblUserName.text = user.name
                self.loadImage(stringUrl: user.avatar, placeHolder: UIImage(named: "leChateauLogo"), imgView: self.imgUserAvater)
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func acceptBag(id: Int) {
        K_Network.sendRequest(function: apiService.acceptBag(id: id), showLoading: true ,success: { (code, msg, response)  in
//            do {
//                self.currentEvents = try response.map(to: [Event].self, keyPath: "data")
            self.btnAcceptBag.isUserInteractionEnabled = false
            self.btnAcceptBag.backgroundColor = UIColor.init(named: "7ABD59")
            self.btnAcceptBag.setTitle("delivered".localized(), for: .normal)
            self.btnAcceptBag.setImage(UIImage(named: "12608_active_check_checkmark_correct_done_icon"), for: .normal)
//            } catch {
//            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
}
