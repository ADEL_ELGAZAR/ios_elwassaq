//
//  NotificationsVC.swift
//  lechateau
//
//  Created by ADEL ELGAZAR on 12/04/2024.
//

import UIKit
import Toaster

class NotificationsVC: ParentVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    var notifications: [ShowNotification] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NotificationTVC", bundle: nil), forCellReuseIdentifier: "NotificationTVC")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showNotifications()
        readAllNotifications()
    }
    
    @IBAction func btnDeleteNotifications(_ sender: UIButton){
        deleteAllNotifications()
    }
}

extension NotificationsVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTVC", for: indexPath) as? NotificationTVC else {return UITableViewCell()}
        cell.configureNotification(notification: notifications[indexPath.row])
        return cell
    }
}


extension NotificationsVC {
    func showNotifications() {
        K_Network.sendRequest(function: apiService.showNotifications, showLoading: true ,success: { (code, msg, response)  in
            do {
                self.notifications = try response.map(to: [ShowNotification].self, keyPath: "notifications")
                self.tableView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func readAllNotifications() {
        K_Network.sendRequest(function: apiService.readAllNotifications, showLoading: false ,success: { (code, msg, response)  in
            do {
                let _ = try response.map(to: Success.self)
                UIApplication.shared.applicationIconBadgeNumber = 0
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func deleteAllNotifications() {
        K_Network.sendRequest(function: apiService.deleteAllNotifications, showLoading: true ,success: { (code, msg, response)  in
            do {
                let response = try response.map(to: Success.self)
                Toast(text: response.message).show()
                self.showNotifications()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
}
