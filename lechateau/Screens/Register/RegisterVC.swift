//
//  RegisterVC.swift
//  lechateau
//
//  Created by mac on 04/03/2024.
//

import UIKit
import Toaster

class RegisterVC: ParentVC, UITextFieldDelegate {
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtJob_title: UITextField!
    @IBOutlet weak var txtSpecilty_type: UITextField!
    @IBOutlet weak var txtClassification_job_id: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPassword_confirmation: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            return (textField.text! as NSString).replacingCharacters(in: range, with: string).isValidSaudiPhone()
        }
        return true
    }
    
    @IBAction func btnSignUpPressed(_ sender: UIButton){
        let language = K_Defaults.string(forKey: Saved.language) ?? "en"
        
        if language.elementsEqual("en") {
            register(params: RegisterInputParams(title: txtTitle.text ?? "", name: txtName.text ?? "", title_ar: "", name_ar: "", phone: txtPhone.text ?? "", job_title: txtJob_title.text ?? "", specilty_type: txtSpecilty_type.text ?? "", classification_job_id: txtClassification_job_id.text ?? "", email: txtEmail.text ?? "", password: txtPassword.text ?? "", password_confirmation: txtPassword_confirmation.text ?? "", locale: language))
        } else {
            register(params: RegisterInputParams(title: "", name: "", title_ar: txtTitle.text ?? "", name_ar: txtName.text ?? "", phone: txtPhone.text ?? "", job_title: txtJob_title.text ?? "", specilty_type: txtSpecilty_type.text ?? "", classification_job_id: txtClassification_job_id.text ?? "", email: txtEmail.text ?? "", password: txtPassword.text ?? "", password_confirmation: txtPassword_confirmation.text ?? "", locale: language))
        }
    }
    
    @IBAction func btnShowPassword(_ sender: UIButton){
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    @IBAction func btnShowConfirmPassword(_ sender: UIButton){
        txtPassword_confirmation.isSecureTextEntry = !txtPassword_confirmation.isSecureTextEntry
    }
    
    @IBAction func btnLoginPressed(_ sender: UIButton){
        dismiss(animated: true)
    }
    
}

extension RegisterVC {
    func register(params: RegisterInputParams) {
        K_Network.sendRequest(function: apiService.register(params: params), showLoading: true ,success: { (code, msg, response)  in
            do {
                let message = try response.map(to: Success.self)
                print("\(message.message)")
                let vc = VerficationVC()
                vc.modalPresentationStyle = .fullScreen
                vc.email = self.txtEmail.text ?? ""
                self.present(vc, animated: true)
            } catch {
                print("Error Here")
            }
            
        }) { (code, msg, response, errors) in
            if errors?.title.first != nil {
                Toast(text: errors?.title[0]).show()
            }
            else if errors?.name.first != nil {
                Toast(text: errors?.name[0]).show()
            }
            else if errors?.phone.first != nil {
                Toast(text: errors?.phone[0]).show()
            }
            else if errors?.job_title.first != nil {
                Toast(text: errors?.job_title[0]).show()
            }
            else if errors?.specilty_type.first != nil {
                Toast(text: errors?.specilty_type[0]).show()
            }
            else if errors?.classification_job_id.first != nil {
                Toast(text: errors?.classification_job_id[0]).show()
            }
            else if errors?.email.first != nil {
                Toast(text: errors?.email[0]).show()
            }
            else if errors?.password.first != nil {
                Toast(text: errors?.password[0]).show()
            } else {
                Toast(text: msg).show()
            }
        }
    }
}
