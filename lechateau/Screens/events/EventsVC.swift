//
//  EventsVC.swift
//  lechateau
//
//  Created by mac on 10/03/2024.
//

import UIKit
import Toaster
import SYBadgeButton

class EventsVC: ParentVC {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnNotification: SYBadgeButton!
    var myEvents: [Event] = []
    var allEvents: [Event] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        tableView.register(UINib(nibName: "EventTVC", bundle: nil), forCellReuseIdentifier: "EventTVC")
        getAllEvents()
        getMyEvents()
    }
    
    func setupUI(){
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "#468FAB") ?? UIColor.black], for: .normal)
        if let notifications = K_Defaults.string(forKey: Saved.notifications), !notifications.isEmpty {
            btnNotification.badgeValue = notifications
        }
        btnNotification.setImage(UIImage(named: "notification 1"), for: .normal)
        
        segmentedControl.setTitle("my_events".localized(), forSegmentAt: 0)
        segmentedControl.setTitle("all_events".localized(), forSegmentAt: 1)
    }

    @IBAction func didChangeSegment(_ sender: UISegmentedControl){
        tableView.reloadData()
    }
    
    @IBAction func btnNotificationsPressed(_ : UIButton) {
        let vc = NotificationsVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
}

extension EventsVC: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            return myEvents.count
        } else {
            return allEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EventTVC", for: indexPath) as? EventTVC else {return UITableViewCell()}
        if segmentedControl.selectedSegmentIndex == 0 {
            if let event = myEvents[indexPath.row].event {
                cell.configure(event: event)
            }
        } else {
            cell.configure(event: allEvents[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = WebVC()
        if segmentedControl.selectedSegmentIndex == 0 {
            if let event = myEvents[indexPath.row].event {
                vc.site_event_link = event.site_event_link
            }
        } else {
            vc.site_event_link = allEvents[indexPath.row].site_event_link
        }
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}


extension EventsVC {
    func getAllEvents() {
        K_Network.sendRequest(function: apiService.getAllEvents, showLoading: true ,success: { (code, msg, response)  in
            do {
                self.allEvents = try response.map(to: [Event].self, keyPath: "data")
                self.tableView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
    
    func getMyEvents() {
        K_Network.sendRequest(function: apiService.getMyEvents, showLoading: true ,success: { (code, msg, response)  in
            do {
                self.myEvents = try response.map(to: [Event].self, keyPath: "data")
                self.tableView.reloadData()
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            Toast(text: msg).show()
        }
    }
}
