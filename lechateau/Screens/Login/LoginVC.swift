//
//  LoginVC.swift
//  lechateau
//
//  Created by mac on 04/03/2024.
//

import UIKit
import FirebaseDynamicLinks
import Toaster

class LoginVC: ParentVC {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func btnLoginPressed(_ sender: UIButton) {
        login(params: LoginInputParams(email: txtEmail.text ?? "", password: txtPassword.text ?? ""))
    }
    
    @IBAction func btnLoginAsVisitorPressed(_ sender: UIButton) {
        login(params: LoginInputParams(email: "adelelgazar93+10@gmail.com", password: "password"))
    }
    
    @IBAction func btnSignUpPressed(_ sender: UIButton) {
        let vc = RegisterVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func btnForgetPasswordPressed(_ sender: UIButton) {
        let vc = ForgetPasswordVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func btnShowPasswordPressed(_ sender: UIButton) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
}

extension LoginVC {
    
    func login(params: LoginInputParams) {
        K_Network.sendRequest(function: apiService.login(params: params), showLoading: true ,success: { (code, msg, response)  in
            do {
                let user = try response.map(to: User.self, keyPath: "data")
                K_Defaults.set(user.name, forKey: Saved.name)
                K_Defaults.set(user.phone, forKey: Saved.phone)
                K_Defaults.set(user.token, forKey: Saved.api_token)
                K_Defaults.set(user.email, forKey: Saved.email)
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"tabController")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            } catch {
            }
            
        }) { (code, msg, response, errors) in
            if errors?.email.first != nil {
                Toast(text: errors?.email[0]).show()
            }
            else if errors?.password.first != nil {
                Toast(text: errors?.password[0]).show()
            }
        }
    }
    
  
}
