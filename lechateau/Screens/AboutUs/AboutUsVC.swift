//
//  AboutUsVC.swift
//  lechateau
//
//  Created by mac on 08/04/2024.
//

import UIKit
import Moya
import Moya_ModelMapper

class AboutUsVC: ParentVC {
    @IBOutlet weak var lblOurStoryTitle: UILabel!
    @IBOutlet weak var lblOurStory: UILabel!
    
    @IBOutlet weak var lblOurVisionTitle: UILabel!
    @IBOutlet weak var lblOurVision: UILabel!
    
    @IBOutlet weak var lblOurMsgTitle: UILabel!
    @IBOutlet weak var lblOurMsg: UILabel!
    
    @IBOutlet weak var lblOurMissionTitle: UILabel!
    @IBOutlet weak var lblOurMission: UILabel!
    
    @IBOutlet weak var lblOurValuesTitle: UILabel!
    @IBOutlet weak var lblOurValues: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getAbout()
    }
    
    func getAbout() {
        K_Network.sendRequest(function: apiService.getAbout, showLoading: true, success: { (code, msg, response)  in
            do {
                let about = try response.map(to: About.self, keyPath: "data")
                
                self.lblOurStoryTitle.text = "OurStory".localized()
                self.lblOurVisionTitle.text = "OurVision".localized()
                self.lblOurMsgTitle.text = "OurMsg".localized()
                self.lblOurMissionTitle.text = "OurMission".localized()
                self.lblOurValuesTitle.text = "OurValues".localized()
                
                self.lblOurStory.text = about.ourstory.html2String
                self.lblOurVision.text = about.ourvision.html2String
                self.lblOurMsg.text = about.ourmsg.html2String
                self.lblOurMission.text = about.ourmission.html2String
                self.lblOurValues.text = about.ourvalues.html2String
            } catch {
            }
            
        }) { (code, msg, response, errors) in
        }
    }
}
