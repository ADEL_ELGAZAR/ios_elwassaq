//
//  PromoVC.swift
//  lechateau
//
//  Created by ADEL ELGAZAR on 10/04/2024.
//

import UIKit
import AVKit

class PromoVC: ParentVC {

    var countdownTimer: Timer!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        playVideo(from: "Promo.mp4")
    }
    
    private func playVideo(from file:String) {
        let file = file.components(separatedBy: ".")
        
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        player.isMuted = false
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = view.bounds
//        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(playerLayer)
        player.play()
        countdownTimer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        countdownTimer.invalidate()
        CheckVersion(version: AppInfo().build)
    }
    
    func skipSplashVC(){
        if K_Defaults.string(forKey: Saved.api_token) != nil {
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabController")
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        } else if K_Defaults.string(forKey: Saved.language) != nil {
            let vc = LoginVC()
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        } else {
            let vc = ChangeLanguageVC()
            vc.backHidden = true
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }
    }
    
    func showUpdateDialog() {
        let updatePopup: UpdatePopup! = UpdatePopup.instanceFromNib()
        updatePopup.frame = self.view.bounds
        self.view.addSubview(updatePopup)
        self.view.bringSubviewToFront(updatePopup)
                
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            updatePopup.container.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        }) {_ in
            updatePopup.container.transform = CGAffineTransform.identity
        }
    }
}

extension PromoVC {
    
    func CheckVersion(version: String) {
        K_Network.sendRequest(function: apiService.CheckVersion(version: version), showLoading: false, success: { (code, msg, response)  in
            do {
                let version = try response.map(to: AppVersion.self)
                if version.data.elementsEqual("yes") {
                    self.showUpdateDialog()
                } else {
                    // open another activity
                    self.skipSplashVC()
                }
            } catch {
            }
            
        }) { (code, msg, response, errors) in
        }
    }
}
