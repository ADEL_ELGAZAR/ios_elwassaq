//
//  ForgetPasswordVC.swift
//  lechateau
//
//  Created by mac on 05/03/2024.
//

import UIKit
import Toaster

class ForgetPasswordVC: ParentVC {

    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnSendPressed(_ sender: UIButton){
        if txtEmail != nil {
            sendVerifyCode(params: SendEmailVerifyCodeInputParams(email: txtEmail.text ?? "", type: "password"))
        }
    }
}


extension ForgetPasswordVC {
    
    func sendVerifyCode(params: SendEmailVerifyCodeInputParams) {
        K_Network.sendRequest(function: apiService.sendEmailverifyCode(params: params), showLoading: true ,success: { (code, msg, response)  in
            do {
                let message = try response.map(to: Success.self)
                let vc = ResetPasswordVC()
                vc.email = self.txtEmail.text ?? ""
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            } catch {
                print("Error Here")
            }
            
        }) { (code, msg, response, errors) in
            if errors?.email.first != nil {
                Toast(text: errors?.email[0]).show()
            }
        }
    }
}
