//
//  ContactUsVC.swift
//  lechateau
//
//  Created by mac on 08/04/2024.
//

import UIKit

class ContactUsVC: ParentVC {
    
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var lblEmail: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        getContact()
    }
    
    func getContact() {
        K_Network.sendRequest(function: apiService.getContact, showLoading: true, success: { (code, msg, response)  in
            do {
                let contact = try response.map(to: Contact.self, keyPath: "data")
                self.lblAddress.text = contact.address
                self.lblPhone.text = contact.phone
                self.lblEmail.text = contact.email
            } catch {
            }
            
        }) { (code, msg, response, errors) in
        }
    }
}

