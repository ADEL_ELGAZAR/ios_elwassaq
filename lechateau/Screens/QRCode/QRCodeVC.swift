//
//  QRCodeVC.swift
//  lechateau
//
//  Created by mac on 11/03/2024.
//

import UIKit
import SimpleQRCode

class QRCodeVC: ParentVC {
    
    @IBOutlet weak var codeImage: UIImageView!
    @IBOutlet weak var parentview: UIView!
    var code = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        parentview.layer.cornerRadius = 20
        let qrCodeB = QRCode(string: code)
        codeImage.image =  try? qrCodeB?.image()
    }

}
