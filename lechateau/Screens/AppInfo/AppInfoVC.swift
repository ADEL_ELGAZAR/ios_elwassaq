//
//  AppInfoVC.swift
//  lechateau
//
//  Created by ADEL ELGAZAR on 11/04/2024.
//

import UIKit

class AppInfoVC: ParentVC {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func btnStartPressed(_ sender: UIButton) {
        K_Defaults.set(true, forKey: Saved.infoSkipped)
        let vc = LoginVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}
