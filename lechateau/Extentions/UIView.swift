//
//  UIView.swift
//  lechateau
//
//  Created by mac on 04/04/2024.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        set{
            self.layer.borderWidth = newValue
        }get{
            return self.borderWidth
        }
    }
     
    @IBInspectable var borderColor: UIColor {
        set{
            self.layer.borderColor = newValue.cgColor
        }get{
            return self.borderColor
        }
    }
    
}
