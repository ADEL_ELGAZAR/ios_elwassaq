//
//  NukeExtention.swift
//  lechateau
//
//  Created by mac on 11/03/2024.
//

import Foundation
import UIKit
import Nuke

extension UITableViewCell {
    func loadImage(stringUrl: String, placeHolder: UIImage?, imgView: UIImageView) {
        if !stringUrl.isEmpty, let url = URL(string: stringUrl) {
            let options = ImageLoadingOptions(
                placeholder: placeHolder,
                transition: .fadeIn(duration: 0.0),
                failureImage: placeHolder
            )
            Nuke.loadImage(with: url, options: options, into: imgView)
        }
    }
}

extension UICollectionViewCell {
    func loadImage(stringUrl: String, placeHolder: UIImage?, imgView: UIImageView) {
        if !stringUrl.isEmpty, let url = URL(string: stringUrl) {
            let options = ImageLoadingOptions(
                placeholder: placeHolder,
                transition: .fadeIn(duration: 0.0),
                failureImage: placeHolder
            )
            Nuke.loadImage(with: url, options: options, into: imgView)
        }
    }
}

extension UIViewController {
    func loadImage(stringUrl: String, placeHolder: UIImage?, imgView: UIImageView) {
        if !stringUrl.isEmpty, let url = URL(string: stringUrl) {
            let options = ImageLoadingOptions(
                placeholder: placeHolder,
                transition: .fadeIn(duration: 0.0),
                failureImage: placeHolder
            )
            Nuke.loadImage(with: url, options: options, into: imgView)
        }
    }
}
