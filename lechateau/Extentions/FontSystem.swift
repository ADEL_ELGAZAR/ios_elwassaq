//
//  FontSystem.swift
//
//  Created by hassan on 11/9/20.
//  Copyright © 2020 hassan. All rights reserved.
//

import Foundation
import UIKit


private let familyName = "Inter"

enum AppFontSystem: String {
    
    case semiBold = "SemiBold"
    case medium = "Medium"
    case regular = "Regular"
    case light = "Light"
    case bold = "Bold"

    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size ) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    fileprivate var fullFontName: String {
        if let font = InterFontSystem.init(rawValue: self.rawValue){
            return font.fullFontName
        }
        return InterFontSystem.regular.fullFontName
    }
}

enum InterFontSystem: String {
    
    case extraBold = "ExtraBold"
    case semiBold = "SemiBold"
    case medium = "Medium"
    case regular = "Regular"
    case bold = "Bold"

    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size ){ //+ 1.0) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    fileprivate var fullFontName: String {
        return "Inter-" + rawValue
    }

}



// MARK: - Localizing Font For UIViews
extension UILabel {
 
  
    @IBInspectable
    var semiBold: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.semiBold.size(newValue)
        }
    }
    
    @IBInspectable
    var medium: CGFloat {
        get {
            return fontSize
        } set {
            self.font = InterFontSystem.medium.size(newValue)
        }
    }
    
    @IBInspectable
    var regular: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.regular.size(newValue)
        }
    }
   
    
    @IBInspectable
    var bold: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.bold.size(newValue)
        }
    }
    
    fileprivate var fontSize: CGFloat {
        return font.pointSize
    }
}
extension UITextField {
    
    
    @IBInspectable
    var semiBold: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.semiBold.size(newValue)
        }
    }
    
    @IBInspectable
    var medium: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.medium.size(newValue)
        }
    }
    
    @IBInspectable
    var regular: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.regular.size(newValue)
        }
    }
  
    @IBInspectable
    var bold: CGFloat {
        get {
            return fontSize
        } set {
            self.font = AppFontSystem.bold.size(newValue)
        }
    }
    
    fileprivate var fontSize: CGFloat {
        return font?.pointSize ?? 0
    }
}

extension UIButton {
    
  
    @IBInspectable
    var semiBold: CGFloat {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = AppFontSystem.semiBold.size(newValue)
        }
    }
    
    @IBInspectable
    var medium: CGFloat {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = AppFontSystem.medium.size(newValue)
        }
    }
    
    @IBInspectable
    var regular: CGFloat {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = AppFontSystem.regular.size(newValue)
        }
    }
 
    @IBInspectable
    var bold: CGFloat {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = AppFontSystem.bold.size(newValue)
        }
    }
    
    fileprivate var fontSize: CGFloat {
        return titleLabel?.font.pointSize ?? 0
    }
}
