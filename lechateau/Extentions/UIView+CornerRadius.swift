//
//  UIView+CornerRadius.swift
//  lechateau
//
//  Created by mac on 04/04/2024.
//

import Foundation
import UIKit
extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }
}
